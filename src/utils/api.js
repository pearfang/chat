//import Vue from 'vue'
import axios from 'axios'
import qs from 'qs'
// Vue.prototype.axios = axios
var instance =axios.create({
    baseURL:'https://www.easy-mock.com/mock/5baafbf19f43b479fb7c855e/chat',
    timeout: 1000,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
      }
    
})
//instance.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
instance.interceptors.request.use(config=>{

     return config;
 },err=>{ 
     
 });
 // 响应时
 instance.interceptors.response.use(response => response, err => Promise.resolve(err.response))
export default {
   request(url,params,method="post"){
        return new Promise((resolve,reject)=>{
            instance({
                url:url,
                data:qs.stringify(params),
                method:method
            }).then(res=>{
                resolve(res)
            }).catch(err=>{
                reject(err)
            })
        })
   }
}