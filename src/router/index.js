import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import login from '@/components/login'
import home from '@/components/home/home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld
    },
   /*  {
      path:'/',
      name:'home',
      component:home,
      children:[
        {
          path:'login',
         
          component:login
        }
      ]
    } */
    {
      path:'/',
      name:'home',
      component:home,
    },
    {
        path:'/login',
        name:'login',
        component:login
    }
  ]
})
